# -*- coding: utf-8 -*-
# Part of AYIRA MARITIME. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Vessel Management',
    'version' : '0.1',
    'author'  : 'Ighor Jesse',
    'category': 'Human Resources',
    'website' : 'https://vessel.ayira.net',
    'summary' : 'Vessel, certificates, insurances, costs',
    'description' : """
Vessel, certificates, insurances, cost
==================================
With this module, AYIRA  MARITIME helps you managing all your vessels, the
certificates associated to those vessel as well as services, fuel log
entries, costs and many other features necessary to the management 
of your freight of vessel(s)

Main Features
-------------
* Add vessels to your freight
* Manage certificates for vessels
* Reminder when a certificate reach its expiration date
* Add services, fuel log entry, speed values for all vessels
* Show all costs associated to a vessel or to a type of service
* Analysis graph for costs
""",
    'depends': [
        'base',
        'mail',
    ],
    'data': [
        'security/freight_security.xml',
        'security/ir.model.access.csv',
        'views/freight_vessel_model_views.xml',
        'views/freight_vessel_views.xml',
        'views/freight_vessel_cost_views.xml',
        'views/freight_board_view.xml',
        # 'data/freight_ship_data.xml',
        # 'data/freight_data.xml',
    ],

    # 'demo': ['data/freight_demo.xml'],

    'installable': True,
    'application': True,
}
