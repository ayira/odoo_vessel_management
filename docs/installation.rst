.. highlight:: shell

============
Installation
============


Stable release
--------------

To install odoo vessel management, run this command in your terminal:

.. code-block:: console

    $ pip install odoo_vessel_management

This is the preferred method to install odoo vessel management, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for odoo vessel management can be downloaded from the `Github repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://github.com/ayira/odoo_vessel_management

Or download the `tarball`_:

.. code-block:: console

    $ curl  -OL https://github.com/ayira/odoo_vessel_management/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Github repo: https://github.com/ayira/odoo_vessel_management
.. _tarball: https://github.com/ayira/odoo_vessel_management/tarball/master
